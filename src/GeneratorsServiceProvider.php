<?php

namespace Mwc\Generators;

use Blade;
use Illuminate\Support\ServiceProvider;

class GeneratorsServiceProvider extends ServiceProvider {


    /**
     * Booting
     */
    public function boot()
    {
        Blade::directive('php', function() {
            return "<?php echo '<?php'.PHP_EOL; ?>";
        });

        Blade::directive('endphp', function() {
            return "<?php echo '?>'; ?>";
        });
    }

	/**
	 * Register the commands
	 *
	 * @return void
	 */
	public function register()
	{
        foreach([
            'Controller'] as $command)
        {
            $this->{"register$command"}();
        }
	}

    /**
     * Register the controller generator
     */
    protected function registerController()
    {
        $this->app->singleton('generate.controller', function ($app) {
            return $app['Mwc\Generators\Commands\ControllerGeneratorCommand'];
        });

        $this->commands('generate.controller');
    }

}
