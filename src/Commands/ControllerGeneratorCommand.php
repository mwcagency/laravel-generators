<?php namespace Mwc\Generators\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\AppNamespaceDetectorTrait;
use Illuminate\Filesystem\Filesystem;

class ControllerGeneratorCommand extends Command
{

    use AppNamespaceDetectorTrait;

    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'generate:controller {name?} {--blank} {--no-office} {--office=Backend}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a controller';

    /**
     * Meta information for the requested command.
     *
     * @var array
     */
    protected $type = 'controller';

    /**
     * The filesystem instance.
     *
     * @var Filesystem
     */
    protected $files;

    /**
     * Meta information for the requested command.
     *
     * @var array
     */
    protected $meta;

    /**
     * Create a new command instance.
     *
     * @param Filesystem $files
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    public function handle()
    {
        $this->makeController();
    }

    protected function handleMeta()
    {
        if(!($this->meta['name'] = $this->argument('name')))
        {
            $this->meta['name'] = $this->ask('what\'s name of your controller ?');
        }

        $this->meta['office'] = $this->option('no-office') ? false : $this->option('office');

        $this->meta['options']['blank'] = $this->option('blank');

        $this->setMeta();
    }

    protected function setMeta()
    {
        if(($office = $this->meta['office']))
        {
            $this->meta['namespace'] = $this->getAppNamespace() . 'Http\\Controllers\\' . ucfirst($office);
        }
        else {
            $this->meta['namespace'] = $this->getAppNamespace() . 'Http\\Controllers';
        }

        $this->meta['class'] = ucfirst($this->meta['name']) . ucfirst($this->type);
    }

    /**
     * Generate the desired migration.
     */
    protected function makeController()
    {
        $this->handleMeta();

        if ($this->files->exists($path = $this->getPath($this->meta['class']))) {
            return $this->error($this->meta['name'] . ' ' . $this->type . ' already exists!');
        }

        $this->makeDirectory($path);

        $this->files->put($path, $this->compileControllerStub());

        $this->info('Controller created successfully.');
    }

    /**
     * Get the path to where we should store the migration.
     *
     * @param  string $name
     * @return string
     */
    protected function getPath($name)
    {
        if(($office = $this->meta['office']))
        {
            return app_path() . '/Http/controllers/'. ucfirst($office) .'/' . $name . '.php';
        } else {
            return app_path() . '/Http/controllers/' . $name . '.php';
        }
    }

    /**
     * Build the directory for the class if necessary.
     *
     * @param  string $path
     * @return string
     */
    protected function makeDirectory($path)
    {
        if (!$this->files->isDirectory(dirname($path))) {
            $this->files->makeDirectory(dirname($path), 0777, true, true);
        }
    }

    /**
     * Compile the migration stub.
     *
     * @return string
     */
    protected function compileControllerStub()
    {
        return view('controller', $this->meta)->render();
    }
}
